<?php

namespace App\Repository;

use App\Entity\ActiveProject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ActiveProject|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActiveProject|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActiveProject[]    findAll()
 * @method ActiveProject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActiveProjectsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActiveProject::class);
    }

    // /**
    //  * @return ActiveProject[] Returns an array of ActiveProject objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ActiveProject
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
