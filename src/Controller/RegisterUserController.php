<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserRegisterType;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class RegisterUserController extends AbstractController {
    /**
     * @Route("/register", name="register_user")
     */
    public function index(
    ) {

        return $this->render('register_user/index.html.twig', [
            'controller_name' => 'RegisterUserController',
        ]);
    }

    /**
     * @Route("/register/user", name="register_user")
     */
    public function create(
        Request $request,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $user = new User();
        $form = $this->createForm(UserRegisterType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setPassword(
                $passwordEncoder->encodePassword($user, $form->get('password')->getData())
            );
            $em->persist($user);
            $em->flush();
        }

        return $this->render('register_user/index.html.twig', ['form' => $form->createView()]);
    }
}
