<?php

namespace App\Controller;

use App\Repository\ActiveProjectsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class HomepageController extends AbstractController {
    /**
     * @Route("/", name="homepage")
     */
    public function index(
        ActiveProjectsRepository $activeProjectsRepository
    ) {
        $userProjects = $activeProjectsRepository->findBy(['admin' => $this->getUser()]);

        return $this->render('homepage/index.html.twig', [
            'controller_name' => 'HomepageController',
            'user' => $this->getUser(),
            'projects' => $userProjects
        ]);
    }
}
