<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(
 *      name="user",
 *      uniqueConstraints={@ORM\UniqueConstraint(columns={"username", "email"})}
 * )
 * @UniqueEntity(
 *      fields={"username","email"},
 *      message="Username or email already exists!"
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=191)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=191)
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity=ActiveProject::class, mappedBy="admin")
     */
    private $myActiveProjects;

    /**
     * @ORM\ManyToMany(targetEntity=ActiveProject::class, mappedBy="users")
     */
    private $activeProjects;

    public function __construct()
    {
        $this->myActiveProjects = new ArrayCollection();
        $this->activeProjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|ActiveProject[]
     */
    public function getMyActiveProjects(): Collection
    {
        return $this->myActiveProjects;
    }

    public function addMyActiveProject(ActiveProject $myActiveProject): self
    {
        if (!$this->myActiveProjects->contains($myActiveProject)) {
            $this->myActiveProjects[] = $myActiveProject;
            $myActiveProject->setAdmin($this);
        }

        return $this;
    }

    public function removeMyActiveProject(ActiveProject $myActiveProject): self
    {
        if ($this->myActiveProjects->contains($myActiveProject)) {
            $this->myActiveProjects->removeElement($myActiveProject);
            // set the owning side to null (unless already changed)
            if ($myActiveProject->getAdmin() === $this) {
                $myActiveProject->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ActiveProject[]
     */
    public function getActiveProjects(): Collection
    {
        return $this->activeProjects;
    }

    public function addActiveProject(ActiveProject $activeProject): self
    {
        if (!$this->activeProjects->contains($activeProject)) {
            $this->activeProjects[] = $activeProject;
            $activeProject->addUser($this);
        }

        return $this;
    }

    public function removeActiveProject(ActiveProject $activeProject): self
    {
        if ($this->activeProjects->contains($activeProject)) {
            $this->activeProjects->removeElement($activeProject);
            $activeProject->removeUser($this);
        }

        return $this;
    }

    public function getRoles() {
        return ['USER'];
    }

    public function getSalt() {
        return 'whatIsSalt?';
    }

    public function eraseCredentials() {
        // TODO: Implement eraseCredentials() method.
    }


}
